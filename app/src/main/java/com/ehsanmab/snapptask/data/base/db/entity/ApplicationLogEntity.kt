package com.ehsanmab.snapptask.data.base.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "App_LOgTable")
class ApplicationLogEntity {
    val date:Long = 0
    val message:String = ""
    @PrimaryKey(autoGenerate = true) var id:Long = 0

}