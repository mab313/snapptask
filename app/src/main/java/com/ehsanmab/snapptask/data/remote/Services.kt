package com.ehsanmab.snapptask.data.remote

import retrofit2.Call
import retrofit2.http.POST

interface Services {

    @POST("/api/test")
    suspend fun getData():Call<String>

}