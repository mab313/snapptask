package com.ehsanmab.snapptask.data.base.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ehsanmab.snapptask.data.base.db.dao.ApplicationLogDao
import com.ehsanmab.snapptask.data.base.db.entity.ApplicationLogEntity

@Database(entities = [ApplicationLogEntity::class] ,version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun appLogDataAccess() : ApplicationLogDao
}