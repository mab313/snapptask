package com.ehsanmab.snapptask.data.base.db.dao

import androidx.room.Dao
import androidx.room.Insert
import com.ehsanmab.snapptask.data.base.db.entity.ApplicationLogEntity

@Dao
interface ApplicationLogDao {

    @Insert
    suspend fun insertLog(logEntity: ApplicationLogEntity) : Long
}