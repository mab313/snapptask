package com.ehsanmab.snapptask.di

import com.ehsanmab.snapptask.BuildConfig
import com.ehsanmab.snapptask.data.remote.Services
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun providesGson() : GsonConverterFactory = GsonConverterFactory.create(GsonBuilder().create())

    @Singleton
    @Provides
    fun provideRetrofit(gsonConverterFactory: GsonConverterFactory) : Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .build()

    @Singleton
    @Provides
    fun getSpecificApiFile(retrofit: Retrofit) : Services = retrofit.create(Services::class.java)
}