package com.ehsanmab.snapptask.di

import android.content.Context
import androidx.room.Room
import com.ehsanmab.snapptask.BuildConfig
import com.ehsanmab.snapptask.data.base.db.AppDataBase
import com.ehsanmab.snapptask.data.base.db.dao.ApplicationLogDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {

    @Provides
    @Singleton
    fun provideApplicationDataBase(@ApplicationContext appContext:Context) : AppDataBase =
        Room.databaseBuilder(appContext,
            AppDataBase::class.java,
            BuildConfig.DATA_BASE_NAME)
            .build()

    @Provides
    @Singleton
    fun provideLogDao(appDataBase:AppDataBase) : ApplicationLogDao = appDataBase.appLogDataAccess()
}