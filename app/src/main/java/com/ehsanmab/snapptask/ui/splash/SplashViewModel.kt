package com.ehsanmab.snapptask.ui.splash

import com.ehsanmab.snapptask.data.SplashRepositoryImp
import com.ehsanmab.snapptask.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor
    (private val repository: SplashRepositoryImp) :
    BaseViewModel() {

}